#pragma once

template<class T>
class RBNode
{
    T value;
};

template<class T>
class RBTree
{
    T* insert(T&& value);
    bool remove(T&& value);
};