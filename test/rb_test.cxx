#include "gtest/gtest.h"

TEST(FactorialTest, HandlesZeroInput) {
    const char* a = "1234";
    const char  b[] = { '1', '2', '3', '4' };

    size_t size_a = sizeof(a);
    size_t size_b = sizeof(b);
    ASSERT_EQ(size_a, size_b);
}